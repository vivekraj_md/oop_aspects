#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;

class Luggagebag {
protected: // [1] Access specifiers such as protected, private and public
string id;
private:
	int numberlock;
friend void show(Luggagebag &lg); //[7]. friend function
public:
	virtual void display();
	Luggagebag (string, int);
	virtual ~Luggagebag();
Luggagebag( const Luggagebag& src); // [8].copy constructor
virtual void bagcategory() = 0; //[3]. abstract
};
Luggagebag::Luggagebag( string id,int numberlock):
id(id), numberlock(numberlock){
	cout <<" [ The luggage named "<< id << " is created] "<< endl;
}

Luggagebag::Luggagebag(const Luggagebag &src) //[8]. modified copy constructor
{
	this->numberlock = src.numberlock + 4;
	this->id = src.id;
	cout <<" [ Copy of the luggage named "<< id << " is created] "<< endl;
}
void Luggagebag::display(){
	cout <<" The name of the luggage bag is "<< id <<" and lock code "<< numberlock << endl;
}
Luggagebag::~Luggagebag(){
	cout <<" [ The luggage named "<< id<< " is destroyed] "<< endl;
}
void dip(const int& y){ //const reference
}

class Dufflebag: public Luggagebag{
protected:
	float dim;
int dimension=static_cast<int>(dim); // [12]. Explicit type cast
int weight;
static int nextsales; //[15].static states
int sales;
private:
	void saleslist();
public:
	virtual void display();
	Dufflebag( string id, int numberlock,int dimension, int weight);
	virtual ~Dufflebag();
	virtual void bagcategory()
	{
cout << " Large size bag "<< endl; //[3]. abstract class
}
};
int Dufflebag::nextsales = 22; //[15].static class
Dufflebag::Dufflebag(string id, int numberlock, int dimension, int weight):
Luggagebag(id, numberlock), dimension(dimension),weight(weight){
	cout <<" [The duffle bag named "<< id <<" is created] "<< endl;
	saleslist();
}
void Dufflebag::saleslist(){ //[15]. static states
	sales = nextsales++;
}
void Dufflebag::display(){
cout <<" The name of the duffel bag is "<< id << endl; //virtual binding
cout << "with the dimension "<< dimension <<" and weight " << weight << endl;
cout << " The number of bags sold are "<< sales <<endl; //static
}
Dufflebag::~Dufflebag(){ // [9]. destructor
	cout <<" [The duffle bag named "<< id <<" is destroyed] "<< endl;
}
void cast(int& bagtyre){
	bagtyre= 2;
	bagtyre++;
	cout<< " The tyres in the bag"<< bagtyre<<endl;
}

class Bagpacks:public Luggagebag{
private:
	string shp;
public:
	void display();
	Bagpacks(string id, int numberlock, string shp);
	virtual void bagcategory(){
		cout <<" Medium Size bag" << endl;
	}
};
Bagpacks::Bagpacks(string id, int numberlock, string shp):
Luggagebag(id, numberlock),shp(shp){}
void Bagpacks::display(){
	Luggagebag::display();
	cout << "The shape of the bag is "<< shp<< endl;
}

class Carrybag{ // [2]. over loading using methods
protected:
	OOP Assignment-1
	int res;
public:
	void add(float bagx, float bagy);
	void add(int bagx, int bagy);
	int getResult(){
		return this->res;
	}
	void display();
int onesize= 34; //[5]. Multiple Inheritence
void adisplay(){
	cout <<" The size of the zip one is "<< onesize << endl;
}
};
void Carrybag::display(){ // [2]. overloading operation
	cout<< "The result of size is "<< res <<endl;
}
void Carrybag::add( float bagx, float bagy){
	this->res=bagx+bagy;
}
void Carrybag::add(int bagx, int bagy){ // [10]. using 3 overloaded operator +,=,==
	if( bagx== bagy){
		res = bagx+bagy;}
		else{
			res = bagx;
		}
	}
struct Gunnybag{ //struct in c++, public is default declared. point 16
int twosize= 45; //[5]. Multiple inheritence
void bdisplay(){
	cout << "The size of zip two is "<< twosize <<endl;}
void dispmaterial(){ //[4]. over ridding function
	cout << "High quality material" << endl;
}
};

class Bagzip:public Carrybag, public Gunnybag{ //[5]. Multiple Inheritence
public:
	void xdisplay(){
		int size =onesize+twosize;
		cout <<"The total size of the zip is " << size <<endl;
	}
	void dispmaterial(){
cout <<"Average quality material "<< endl; //[4]. over ridding
}
};
template<class bag> //[18].class template
class capacity{
	bag values[20];
public:
	bag& operator [](int index){
		return values[index];
	}
};
void show(Luggagebag& lg){ //[7]. friend function with modification
	string lug= lg.id;
	string lugone= lug +" and wallet ";
	cout <<" The details of "<< lugone <<" is displayed "<< endl;
}
void f(Dufflebag v) //[8].pass by value
{
	v.display();
}
void g(Dufflebag &u) //[8].pass by reference
{
	u.display();
}
struct func{ // sort
	void operator() (int wx){
		cout << wx<< endl;
	}
};
void exp(int& xa){ //explicit cast
	xa= 29;
	++xa;
	cout<< " Value is "<< xa<<endl;
}


int main()
{
	Dufflebag r(" Micky's bag ", 23435, 34, 126);
	r.display();
f(r); //pass by value
g(r); //pass by reference
show(r); //friend function
Carrybag sof; //[2]. overloading function
sof.add(22.0f,33.0f);
sof.display();
sof.add(22,33);
sof.display();

Dufflebag bags[2]=
	{ //[11]. array of objects
		Dufflebag(" Naruto's bag",28758, 44, 124),
		Dufflebag(" Vicky's bag", 44352, 55, 147)
	};
bags[0].display();
bags[1].display();
Dufflebag dbgg; //[3]. abstract class
Bagpacks bpss;
Luggagebag *bagone = &dbgg;
Luggagebag *bagtwo = &bpss;
bagone->bagcategory();
bagtwo->bagcategory();
Bagzip ans; //[5]. Multiple Inheritence
ans.adisplay();
ans.bdisplay();
ans.xdisplay();
Gunnybag gb; // [4]. Over ridding of method
gb.dispmaterial();
Bagzip pd;
pd.dispmaterial();
Carrybag *cbg; //[14].new using pointer
cbg= new Carrybag;
cbg->adisplay();
Gunnybag *gbg;
gbg= new Gunnybag;
gbg->bdisplay();
capacity<int> intArray; //class template
capacity<float> floatArray;

for(int i=2;i<8;i++){
	intArray[i]=i*i;
	floatArray[i]=(float)i/3.3;}

for(int i=2;i<8;i++) //
	cout<<" The intArray value of bag is "<< intArray[i]<<" and the floatArray value of bag is "<<

floatArray[i] << endl;
int wx[10]; //[19]vector
vector <int> vect;
cout <<" The list of bags :" << endl;
wx[0]= 23;
vect.push_back(wx[0]);
wx[1]=9;
vect.push_back(wx[1]);
wx[2]=4;
vect.push_back(wx[2]);
sort(vect.begin(),vect.end()); // [20].sort algorithm
cout<< " The sorted array :";

for(int i=0;i<3;i++){
	cout<<wx[i];
}
for_each(vect.begin(), vect.end(), func());
int yy=78;
dip(yy);
cout<<yy<<endl;
Luggagebag *lb = &r; //[13].virtual dynamic binding
lb->display();
Gunnybag *gn = &gb;// [13].Non virtual binding
gn->bdisplay();
Luggagebag *ya= new Dufflebag; //[12].explicit cast
Dufflebag *xa=dynamic_cast<Dufflebag*>(ya);
const int h=32;
exp(*const_cast<int*>(&h));
cout<<" h has the value" << h <<endl; //[12].explicit cast
ya = new Luggagebag; //[12]. explicit cast
ya->numberlock = 2365;
long numlck=reinterpret_cast<long>(ya);
cout<< " The num lock is "<< numlck <<endl;
void displaySomething(const Luggagebag &luggage) { //[17].const reference
	luggagebag.display();
}

delete cbg; //[14]. delete operation
delete gbg;
return 0;
}